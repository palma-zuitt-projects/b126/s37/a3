let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	let token = localStorage.getItem("token")


if(courseName === '' && description === '' && price){
	alert("Please fill all the blanks")
}else{
	fetch('http://localhost:4000/courses', {
		method: "POST",
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			courseName: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {

		if(data.accessToken){

			localStorage.setItem('token', data.accessToken)

			fetch('http://localhost:4000/courses', {
				headers: {
					Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(res => res.json ())
			.then(data => {
				alert("Course successfully registered")
				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin", data.isAdmin)
				window.location.replace("./courses.html")
			})
		}else{
			alert ("Authentication failed, pls try again")
		}
	})


}
	
})

/*
		Activity:

		Create a fetch request to create a new course
			-Similar to user registration, except we dont check for duplicate email

		Make sure that  you send the token with this request
			-Similar to sending the user token in login.js

		If the server responds with "true" for our request, redirect the user to the user to the courses page
			-Similar to redirects in register.js and login.js

		If not, show an error message in an alert
	
	*/